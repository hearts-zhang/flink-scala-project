package zion

import org.apache.flink.api.scala._
import org.apache.flink.streaming.api.scala.DataStream
import org.apache.flink.streaming.api.windowing.time.Time

object Count {
  type FeatureCount = (String, Int)
  def count(lines: DataStream[String], stops: Set[String], window: Time): DataStream[FeatureCount] ={
    lines
      .flatMap(line => line.split(" "))
      .filter(word => !word.isEmpty)
      .map(word => word.toLowerCase)
      .filter(word => !stops.contains(word))
      .map(word => (word,1))
      .keyBy(0)
      .timeWindow(window)
      .sum(1)
  }
}

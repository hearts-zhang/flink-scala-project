package zion.tv;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.streaming.util.serialization.JSONDeserializationSchema;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * @author zion
 * @date 201801
 * consumer configs http://kafka.apache.org/documentation.html#newconsumerconfigs
 * --bootstrap.servers localhost:9092 --zookeeper.connect localhost:2181 --group.id kafka-flink
 * --topic svideo.bootstrap.1
 */
public class WordCount {
  public static void main(String[] args) throws Exception {
    ParameterTool params = ParameterTool.fromArgs(args);

    final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
    DataStream<ObjectNode> source = env.addSource(new FlinkKafkaConsumer011<>(
        params.get("topic"),
        new JSONDeserializationSchema(),
        params.getProperties()
    ));
    source.map(new MapFunction<ObjectNode, String>() {
      @Override
      public String map(ObjectNode n) throws Exception {
        return n.get("fudid").asText();
      }
    }).print();

    env.execute("where is the result");
  }


  //
  // 	User Functions
  //

  /**
   * Implements the string tokenizer that splits sentences into words as a user-defined
   * FlatMapFunction. The function takes a line (String) and splits it into
   * multiple pairs in the form of "(word,1)" (Tuple2<String, Integer>).
   */
  public static final class LineSplitter implements FlatMapFunction<String, Tuple2<String, Integer>> {

    @Override
    public void flatMap(String value, Collector<Tuple2<String, Integer>> out) {
      // normalize and split the line
      String[] tokens = value.toLowerCase().split("\\W+");

      // emit the pairs
      for (String token : tokens) {
        if (token.length() > 0) {
          out.collect(new Tuple2<String, Integer>(token, 1));
        }
      }
    }
  }
}

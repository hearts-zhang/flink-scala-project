# Using Flink #

- install apache-flink to `/opt/flink`
- `rsync -v d196:/opt/flink/conf/flink-conf.yaml /opt/flink/conf`
- add these line to `/etc/hosts`
- `Flink-Cluster`和提交任务的机器之间需要互通
- `Flink-Cluster` 通过`akka://{hostname}:xxxx`访问客户机器, 所以要求136-198能够以设备名称访问到客户机器
- ...

```text
10.1.7.198      10-1-7-198-mjq  d198
10.1.7.197      10-1-7-197-mjq  d197
10.1.7.196      10-1-7-196-mjq  d196
10.1.7.138      10-1-7-138-mjq  d138
10.1.7.137      10-1-7-137-mjq  d137
10.1.7.136      10-1-7-136-mjq  d136
192.168.13.247  zion-247 zion.247 d247 dev-247 dev.247
192.168.13.249  zion-249 zion.249 d249 dev-249 dev.249
```

```shell
./gradlew shadow

./flink-run
``` 

## flink-run

```shell
#!/usr/bin/env bash

BROKERS=${BROKERS:-`use-conf kafka-brokers`}
ZK=${ZK:-`use-conf kafka-zk`}
TOPIC=${TOPIC:-svideo.bootstrap.1}
GID=${GID:-kafka-flink}

flink run build/libs/flink-1.0-SNAPSHOT-all.jar \
      --zookeeper.connect ${ZK} \
      --bootstrap.servers ${BROKERS} \
      --group.id ${GID} \
      --topic ${TOPIC}
```
